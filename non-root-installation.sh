#!/bin/bash

### UNTESTED

cd $HOME;
git clone https://gitlab.com/d.a.v.i.d.m/dotfiles
wget https://github.com/neovim/neovim/releases/download/v0.3.1/nvim.appimage $HOME/bin # change to latest version


echo "
export XDG_CACHE_HOME=$HOME/.cache
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_DIRS=/usr/local/share:/usr/share:/var/lib/snapd/desktop
export XDG_DATA_HOME=$HOME/.local/share

export PATH=$HOME/bin:$PATH
alias nvim=nvim.appimage

export SHELL=/bin/zsh
exec /bin/zsh --login
" >> ~/.profile

source ~/.profile

cd dotfiles;

./dotfiles/install;
./dotfiles/install-plugins.sh;

# git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
# ~/.fzf/install

pip3 install neovim --user
nvim -c "UpdateRemotePlugins"


curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh


sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)" $XDG_CONFIG_HOME/oh-my-zsh
