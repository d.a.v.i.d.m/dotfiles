
export PATH="$HOME"/bin:$HOME/.local:"$HOME"/.local/bin:$PATH:$HOME/.cargo/bin
ZSH_THEME="common"

ENABLE_CORRECTION="true"

COMPLETION_WAITING_DOTS="true"

HISTFILE=~/.cache/zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST # Expire duplicate entries first when trimming history
setopt HIST_IGNORE_DUPS # Don\'t record an entry that was just recorded again
setopt HIST_IGNORE_ALL_DUPS # Delete old recorded entry if new entry is a duplicate
setopt HIST_FIND_NO_DUPS # Do not display a line previously found
setopt HIST_IGNORE_SPACE # Don\'t record an entry starting with a space
setopt HIST_SAVE_NO_DUPS # Don\'t write duplicate entries in the history file
setopt HIST_REDUCE_BLANKS # Remove superfluous blanks before recording entry

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# options set in outside functions are not set globally
setopt LOCAL_OPTIONS

# wayland stuff
# export WAYLAND_DISPLAY= # so alacritty can run
export XKB_DEFAULT_LAYOUT=dvorak;
export XKB_DEFAULT_VARIANT=dvorak;

bindkey -e # for c-xe and c-r
set editing-mode vi

export KEYTIMEOUT=1

infoWithLess() {
  info $1 | less;
}

openNoteFromPartialName() {
  nvim $(find ~/Nextcloud/Notes/ -type f | fzf)
}

alias ls="ls --color=tty"
alias sl="sl -aFlced" # i've never actually mistaken ls for sl, whoever's reading my dotfiles. i just like the animation lol
alias info=infoWithLess;

alias onote=openNoteFromPartialName;

alias search="lynx duckduckgo.com";

alias wifi-test="ping archlinux.org";

alias franz=/usr/bin/franz # firejail blocks otherwise

alias i3lock="i3lock -c 000000";

# open all unstaged files in buffers
alias vim-git='vim $(git status --short | awk " { print $2 } ")';
alias gs='git status'
alias gcam='git commit -am'
alias gd='git diff HEAD'
alias vim=nvim
alias n=nvim
alias nf='nvim $(fzf)'
alias ns='nvim -S'

alias dotfiles='cd ~/dotfiles'
alias cpd="cp -Tabs $HOME/dotfiles/.config $XDG_CONFIG_HOME"
alias weather="curl 'wttr.in/~${ZIP}?u'"
alias xampp='sudo /opt/lampp/lampp $1'
alias nextcloud-cal-sync='vdirsyncer sync; calcurse-caldav'
alias av="sudo freshclam; sudo clamscan -irv --exclude=/proc --exclude=/sys --exclude=/dev --exclude=/media --exclude=/mnt"

export VISUAL=nvim
export EDITOR="$VISUAL"
export PAGER=nvimpager
export TERM_APP=alacritty
export NOTES_DIRECTORY=~/Nextcloud/Notes/
export ZK_PATH=~/Nextcloud/Notes/zettels

export NPM_CONFIG_PREFIX="$XDG_CACHE_HOME"/npm-global # prevent permission errors (https://docs.npmjs.com/getting-started/fixing-npm-permissions)
export PATH="$XDG_CACHE_HOME"/npm-global:/usr/bin/php:$PATH
export PATH="$PATH:$(yarn global bin)"

# get apps to respect xdg
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export GNUPGHOME="$XDG_CONFIG_HOME"/gnupg # also must run gpg2 --homedir "$XDG_CONFIG_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export JAVA_HOME=/usr/lib/jvm/java-18-openjdk
# tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
# export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc # only respected by xinit, not startx
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc

# needed for dark qt theme
[ "$XDG_CURRENT_DESKTOP" = "KDE" ] || [ "$XDG_CURRENT_DESKTOP" = "GNOME" ] || export QT_QPA_PLATFORMTHEME="qt5ct"

umask 0077;

export _FASD_DATA="$XDG_DATA_HOME"/fasd
eval "$(fasd --init auto)"

# so when fasd opens a file, can work within that directory
fasdVimWithAutoCd() {
  nvim -c "execute \"cd \" . expand(\"%:p:h\")" `fasd $@`
}

# load vim session from anywhere. buggy
fasdVimWithSession() {
  nvim -c "execute \"cd \" . expand(\"%:p:h\")" -c "source Session.vim" -c "bwipeout" `fasd $@` -c "bnext"
}

source ~/.zprofile
