const exec = require('child_process').exec;
const path = require('path');

const getSymLinksCmd = exec(`find . -type l -exec echo {} \\; -exec readlink {} \\;`);

console.log('hi');


let curFileName = '';
let i = 1;
getSymLinksCmd.stdout.on('data', (data) => {
	
	let curDataIsSymlinkInfo = (i % 2 == 0);

	if (curDataIsSymlinkInfo) {

		let dataArr = data.split('/');

		let shouldReplaceLink = dataArr[2] == 'styx';

		if (shouldReplaceLink) {

			dataArr[2] = 'styx';
			let fixedSymData = dataArr.join('/');

			// exec(`ln -sfn ${fixedSymData} ${curFileName}`);
			console.log(`ln -sfn ${fixedSymData} ${curFileName}`);
			// console.log(`${fixedSymData}`);
			// console.log(`${curFileName}`);
		}
	}

	i++;
	curFileName = path.resolve(data); // relative -> absolute path

});


getSymLinksCmd.stderr.on('data', (data) => {

	console.log('==========ERROR==========');
	console.log(data);
	console.log('==========ERROR END==========');
});
