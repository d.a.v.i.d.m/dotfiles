This repo contains my dotfiles
`cp -Tabs /absolute/path/.config $XDG_CONFIG_HOME`

`pacman.txt` and `aur.txt` contain lists of packages I installed to make everything work.

### Apps

| Calendar    | khal                                                         |
|-------------|--------------------------------------------------------------|
| Contacts    | khard                                                        |
|-------------|--------------------------------------------------------------|
| Cloud       | NextCloud                                                    |
|-------------|--------------------------------------------------------------|
| Email       | neomutt                                                      |
|-------------|--------------------------------------------------------------|
| Text Editor | neovim                                                       |
|-------------|--------------------------------------------------------------|
| Syncing     | vdirsyncer with systemctl timer                              |
|-------------|--------------------------------------------------------------|
`systemctl edit --user vdirsyncer`

### Installation
This repository uses the [XDG](https://wiki.archlinux.org/index.php/XDG_Base_Directory_support#XDG_Base_Directory_specification)
Run `./install` to install dotfiles
Create a file called `private-info` and export your zip code as ZIP

Thinkpad keyboard backlight can be controlled by fn-space (out of the box, putting here to remind me)

zk

- Dunsctl for do not disturb i3blocks

### Neovim
+ neovim, fd, nodejs, npm
+ If using neovim over ssh, enable clipboard by adding

```bash
Host myhost
    ForwardX11 yes
    ForwardX11Trusted yes
```
to ~/.ssh/config and installing xclip (or other clipboard manager)
https://wiki.archlinux.org/title/Pacman%23Cleaning_the_package_cache#Cleaning_the_package_cache
