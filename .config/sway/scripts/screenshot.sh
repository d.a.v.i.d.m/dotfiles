#!/bin/bash

# name=$(date '+%Y-%m-%d-%T')
# default_name="$HOME/Pictures/screenshots/$name-fullscreen.png"

default_path=$1
default_name=$2
partscreen=$3
original_name="$default_path/$default_name.png"

zenity --question --text="Take screenshot?"

if [ $? == 1 ]
then
    exit
fi

if [ "$partscreen" ]
then
    grim -g "$(slurp)" "$original_name";
else
    grim "$original_name";
fi

selected_name=$(zenity --entry --text="Screenshot name:")

if [ -z "$selected_name" ]
then
    exit
elif [ "$selected_name" == null ]
then
    rm $default_name;
else
    mv "$original_name" "$default_path/$selected_name.png"
fi
