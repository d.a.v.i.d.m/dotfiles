" sets filetype syntax settings for all file types in one go
" https://stackoverflow.com/a/4301809
if !exists("after_autocmds_loaded")
    let after_autocmds_loaded = 1
    autocmd BufNewFile,BufRead * runtime ./common_syntax.vim
endif




