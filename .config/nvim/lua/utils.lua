
local utils = {
    prefix = {
        key = '',
        desc = ''
    }
}

local custom_leaders = {}

-- I generally want groups of functionality to be under same leader.
--  eg, lsp, telescope, groups of related mappings
-- @param name - `leader` if <leader>. matches case insensitively
-- @example:
--  utils.add_new_custom_leader('lspleader', '<localleader>o')
--  utils.nnoremap('<lspleader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>')
function utils.add_new_custom_leader(name, value)
    custom_leaders[name] = value
end

-- prefix is added to all mapping keys, until remove_prefix is called
-- desc_prefix is added to map description
function utils.add_prefix(key_prefix, desc_prefix)
    utils.prefix.key = key_prefix
    utils.prefix.desc = desc_prefix
end

function utils.remove_prefix()
    utils.prefix.key = ''
    utils.prefix.desc = ''
end


local function extract_custom_leaders(key)

    for potential_leader in string.gmatch(key, '<(.*)>') do
        if custom_leaders[potential_leader] then
            key = string.gsub(key, '<'..potential_leader..'>', custom_leaders[potential_leader])
        end
    end

    return key
end

-- @Summary create mapping functions. same arguments as
--   @see `:help nvim_set_keymap`
-- @param user_opts - default: noremap = true, silent = true
for _, map_mode in pairs({ 'n', 'i', 'c', 'v', 't' }) do

    utils[map_mode..'noremap'] = function(key, map, user_opts)

        user_opts = user_opts or {}

        local opts = { noremap = true, silent = true }
        for opt,val in pairs(user_opts) do
            opts[opt] = val
        end

        local prefixed_key = utils.prefix.key .. key
        prefixed_key = extract_custom_leaders(prefixed_key)

        if type(map) == 'function' then
            opts.callback = map
            map = ''
        end

        opts.desc = utils.prefix.desc .. (opts.desc and opts.desc or '')

		vim.api.nvim_set_keymap(map_mode, prefixed_key, map, opts)
	end
end

return utils;
