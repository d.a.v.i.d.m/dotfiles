
-- """"""""""""""""""""""""""
-- "      Built in stuff    "
-- """"""""""""""""""""""""""

local set = vim.opt;

vim.cmd [[
	syntax enable
	filetype plugin indent on
]]

set.background = "dark"
set.foldlevel = 99
set.foldlevelstart = 99
set.foldnestmax = 5
set.ruler = true
set.showcmd = true
set.showmode = false
set.conceallevel = 2

-- basic completion
set.completeopt = "menuone,noselect,noselect" -- for nvim-cmp
set.shortmess = "filmnrxc"
set.backspace = "indent,eol,start"
set.joinspaces = false
set.backup = true
-- set.dictionary:append{"/usr/share/dict/cracklib-small"}
set.complete:append{"k"}


-- xdg
set.directory = vim.fn.stdpath('cache') .. "/swap"
set.backupdir = vim.fn.stdpath('cache') .. "/backup"
-- % restores buffer list if no args, but annoying
set.shada:append{"'1000,f1,n"}

set.undodir = vim.fn.stdpath('cache') .. "/undo"
set.undofile = true
set.history = 50
set.showbreak = ">"


-- file manipulation
set.timeoutlen = 1000
set.incsearch = true
set.hlsearch = true
set.autoread = true
set.hidden = true
set.equalprg = ""
set.formatoptions = "trqnlj"


set.mouse = "a" -- enable mouse

-- spellcheck
set.spelllang = "en_us"
set.spellcapcheck = ""
set.complete:append{"kspell"}
vim.cmd [[
	if filereadable('/usr/share/dict/words')
		set dictionary+=/usr/share/dict/words
	endif
]]

set.tabstop = 2
set.softtabstop = 2   -- Insert four spaces with tab key
set.shiftwidth = 2
set.breakindent = true -- Indent wrapped lines
set.expandtab = true
set.linebreak = true
vim.api.nvim_set_var('tex_conceal', 'abdgms')
set.virtualedit = "block" -- Let me move beyond buffer text in visual block mode

set.smartindent = true
set.copyindent = true
set.preserveindent = true

-- File searching "
--"""""""""""""""""
set.wildmenu = true
set.wildmode = "longest:full,full"
set.path:remove{"/usr/include"} -- seems to be included by default. If anything breaks, this might be the cause
set.path:append{"**"} -- Allows :find to search all subdirectories from current file. @author https://youtu.be/XA2WjJbmmoM?t=7m4s. Use :b with partial name to open previously opened file
set.wildignore:append{"build,lib,node_modules/,.git/"} -- don't look in this
set.wildignorecase = true -- case insensitive search
set.updatetime = 300 -- shorter time for autocompletion to kick in

-- note: changed at times by vim-rooter, but made rooter manual
set.autochdir = false

set.inccommand = "split" -- see all :s results as type

set.ignorecase = true
set.infercase = true
set.smartcase = true -- case-insensitive text searching

-- default show numbers
set.number = true
set.relativenumber = true

-- set.scrolloff = 9999 -- keep cursor at the center of the screen
set.termguicolors = true -- enable True color

set.clipboard = "unnamedplus" -- so can do \"+p and \"+y to paste from system clipboard


--"" latex
vim.cmd [[
	syntax match texStatement /\\item/ conceal cchar=-
	syntax match texStatement /\\begin{itemize}/ conceal cchar= 
	syntax match texStatement /\\end{itemize}/ conceal cchar= 
	syntax match ArrowHead "<=" conceal cchar=≤
]]


