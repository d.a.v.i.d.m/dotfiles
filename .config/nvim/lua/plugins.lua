--''''''''''''''''''''''''''
--       Plugins           '
--''''''''''''''''''''''''''

-- to organize all plugin cmds, defined in other files
vim.cmd [[
augroup plugin_cmds
autocmd!
augroup END
]]

local package_manager = require('lazy')

package_manager.setup({

  -- looks {{{

  {
    "nanozuki/tabby.nvim",
    config = function() require("tabby").setup({
      tabline = require("tabby.presets").tab_with_top_win,
    }) end,
    -- https://github.com/nanozuki/tabby.nvim/issues/59#issuecomment-1147006898
    after = 'mini.nvim'
  },

  {
    "tiagovla/scope.nvim",
    config = function() require("scope").setup() end,
    event = 'BufRead'
  },

  {
    "tversteeg/registers.nvim",
    event = 'BufRead',
  },

  {
    'akinsho/toggleterm.nvim',
    version = '*',
    config = function()
      require'plugin-configs.toggleterm'
    end,
    event = 'BufRead'
  },

  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    config = function() require'plugin-configs.treesitter' end,
    after = 'impatient.nvim',
  },  -- We recommend updating the parsers on update

  {
    'nvim-treesitter/playground',
    after = 'nvim-treesitter'
  },

  {
    'David-Kunz/treesitter-unit',
    config = function()
      -- require"treesitter-unit".toggle_highlighting()
      vim.api.nvim_set_keymap('x', 'iu', ':lua require"treesitter-unit".select()<CR>', {noremap=true})
      vim.api.nvim_set_keymap('x', 'au', ':lua require"treesitter-unit".select(true)<CR>', {noremap=true})
      vim.api.nvim_set_keymap('o', 'iu', ':<c-u>lua require"treesitter-unit".select()<CR>', {noremap=true})
      vim.api.nvim_set_keymap('o', 'au', ':<c-u>lua require"treesitter-unit".select(true)<CR>', {noremap=true})
    end,
    after = 'nvim-treesitter'
  },

  {
    'nvim-treesitter/nvim-treesitter-textobjects',
    after = 'nvim-treesitter'
  },

  -- I get an error when navigating via { in markdown files with the below plugin enabled
  -- {
  --   'romgrk/nvim-treesitter-context',
	 --  config = function() require'treesitter-context'.setup() end,
  --   after = 'nvim-treesitter'
  -- },

  -- {
  --     'nvim-neorg/neorg',
  --     rocks = { "nvim-nio", "nui.nvim", "plenary.nvim", "pathlib.nvim" },
  --     config = function() require('plugin-configs.neorg') end,
  --     -- dependencies = {
  --     --     'folke/zen-mode.nvim',
  --     --     'nvim-lua/plenary.nvim'
  --     -- },
  --     after = 'nvim-treesitter',
  -- },

  -- {
  --     "b0o/schemastore.nvim",
  --     config = function()
  --         require('lspconfig').jsonls.setup {
  --           settings = {
  --             json = {
  --               schemas = require('schemastore').json.schemas(),
  --             },
  --           },
  --     },
  --     end,
  --     after = 'nvim-lspconfig'
  -- },

  {
    'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
    config = function()

      require("lsp_lines").setup()

      vim.diagnostic.config({
        virtual_text = false,
      })
    end,
  },

  {
    'RishabhRD/popfix',
    after = 'nvim-lspconfig'
  },

  {
    "glepnir/lspsaga.nvim",
    branch = "main",
    after = 'nvim-lspconfig',
    config = function() require('plugin-configs.lspsaga') end,
  },

  {
    'RishabhRD/nvim-lsputils',
    after = {
      'nvim-lspconfig',
      'popfix'
    },
    config = function()
      vim.lsp.handlers['textDocument/codeAction'] = require'lsputil.codeAction'.code_action_handler
      vim.lsp.handlers['textDocument/references'] = require'lsputil.locations'.references_handler
      vim.lsp.handlers['textDocument/definition'] = require'lsputil.locations'.definition_handler
      vim.lsp.handlers['textDocument/declaration'] = require'lsputil.locations'.declaration_handler
      vim.lsp.handlers['textDocument/typeDefinition'] = require'lsputil.locations'.typeDefinition_handler
      vim.lsp.handlers['textDocument/implementation'] = require'lsputil.locations'.implementation_handler
      vim.lsp.handlers['textDocument/documentSymbol'] = require'lsputil.symbols'.document_handler
      vim.lsp.handlers['workspace/symbol'] = require'lsputil.symbols'.workspace_handler
    end
  },

  {
    "ThePrimeagen/refactoring.nvim",
    dependencies = {
      {"nvim-lua/plenary.nvim"},
      {"nvim-treesitter/nvim-treesitter"},
    },
    config = function() require('plugin-configs.refactoring') end,
    after = 'nvim-treesitter',
  },

  {
    'lukas-reineke/indent-blankline.nvim',
    config = function()
      require('ibl').setup {
      }
    end,
    after = 'nvim-treesitter',
  },

  {
    'kyazdani42/nvim-tree.lua',
    dependencies = {
      'kyazdani42/nvim-web-devicons', -- optional, for file icon
    },
    config = function() require'nvim-tree'.setup {} end,
    cmd = { 'NvimTreeToggle' },
  },

  { 'beauwilliams/focus.nvim', cmd = { "FocusSplitNicely", "FocusSplitCycle" },
    config = function()
      require("focus").setup({hybridnumber = true})
    end
  },

  -- {
  --     'stevearc/aerial.nvim',
  --     config = function() require'plugin-configs.aerial' end,
  --     event = 'BufRead',
  -- },

  {
    'feline-nvim/feline.nvim',
    config = function()
      require('feline').setup()
      -- require('feline').winbar.setup()
    end,
    event = 'ColorScheme'
  },

  {
    'kevinhwang91/nvim-bqf', -- better quickfix
    event = 'ColorScheme'
  },

  {
    -- editable qf
    'stefandtw/quickfix-reflector.vim',
  },

  -- }}},

  -- git {{{
  {
    'TimUntersberger/neogit',
    dependencies = 'nvim-lua/plenary.nvim',
    cmd = { 'Neogit' },
  },

  {
    'lewis6991/gitsigns.nvim',
    dependencies = 'nvim-lua/plenary.nvim',
    config = function() require'gitsigns'.setup({signcolumn = true}) end,
    event = 'BufRead',
  },
  -- }}},


  -- useful for everything {{{

  {
    'tpope/vim-obsession'
  },

  {
    'dstein64/vim-startuptime',
    cmd = { 'StartupTime' },
  },

  {
    'jghauser/mkdir.nvim',
    event = 'BufRead'
  },

  {
    '0styx0/abbreinder.nvim',
    dependencies = {
      {
        '0styx0/abbremand.nvim',
      },
    },
    config = function()
      -- <config> can be empty to stay with defaults
      -- or anything can be changed, with anything unspecified
      -- retaining the default values
      require'abbreinder'.setup()
    end,
    event = 'BufRead', -- if want lazy load
  },
  -- {
  --     '~/Documents/projects/abbreinder',
  --     dependencies = {
  --         '~/Documents/projects/abbremand.nvim',
  --     },
  --     -- '0styx0/abbreinder.nvim',
  --     config = function()
  --         require'abbreinder'.setup()
  --     end,
  --     event = 'BufRead', -- if want lazy load
  -- },

  {
    'echasnovski/mini.nvim',
    -- branch = 'stable',
    config = function() require'plugin-configs.mini' end,
    dependencies = {'kyazdani42/nvim-web-devicons', lazy = true},
    event = 'BufRead'
  },

  'junegunn/vim-slash', -- stop highlighting searches after move cursor
    {
      'junegunn/goyo.vim',
      cmd = 'Goyo'
    }, -- distraction free writing

    {
      'tpope/vim-abolish', -- easily add abbrs, :s for all mutations of a word. Also has crs (snake'_case), crc (camelCase) cru (UPPER_CASE), cr- (dash-case), and more
      config = function() require('plugin-configs.abolish') end,
      cmd = {
        'Abolish',
        'Subvert',
      },
      -- event = 'BufRead',
    },

    {
      'mbbill/undotree',
      cmd = 'UndotreeToggle',
      config = function() require('plugin-configs.undotree') end
    },

    -- extend mark functionality
    -- {
    --     'ThePrimeagen/harpoon',
    --     dependencies = {
    --         'nvim-lua/plenary.nvim'
    --     },
    --     config = function() require('plugin-configs.harpoon') end
    -- },

    {
      'toppair/reach.nvim',
      config = function() require('plugin-configs.reach') end,
      event = 'BufRead'
    },

    {
      'nvim-telescope/telescope.nvim',
      dependencies = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
      config = function() require('plugin-configs.telescope') end,
      event = 'BufRead', -- if want lazy load
    },

    {
      'nvim-lua/plenary.nvim',
    },

  'editorconfig/editorconfig-vim', -- respect editorconfig files
  -- }}},


  -- language specific things {{{

  -- tex {{
  'lervag/vimtex',

  -- }},

  -- markdown {{
  {
    'iamcco/markdown-preview.nvim',
    build = function() vim.fn['mkdp#util#install']() end,
    ft = 'markdown',
    cmd = { 'MarkdownPreview' },
  },

    -- produces an error when I make new lines
    -- use({
    --     'ixru/nvim-markdown',
    --     ft = 'markdown',
    -- })
    -- }},


    -- motions/textobjects {{{
    {
      'andymass/vim-matchup',
      event = 'ColorScheme',
    },

  {
    "andrewferrier/textobj-diagnostic.nvim",
    config = function()
      require("textobj-diagnostic").setup({create_default_keymaps = false})

      vim.keymap.set({ "x", "o" }, "ad", function()
        require("textobj-diagnostic").next_diag_inclusive(true)
      end, { silent = true })

      vim.keymap.set({ "x", "o" }, "id", function()
        require("textobj-diagnostic").next_diag_inclusive()
      end, { silent = true })

      vim.keymap.set({ "x", "o" }, "pd", function()
        require('textobj-diagnostic').prev_diag()
      end, { silent = true })
    end,
    after = 'nvim-treesitter'
  },

  {
    "kylechui/nvim-surround",
    config = function()
      require("nvim-surround").setup() end,
    event = 'BufRead',
  },

  {
    "gbprod/substitute.nvim",
    config = function()
      require("substitute").setup()
      -- `s<motion>` replaces <motion> with "+, not overwriting
      vim.api.nvim_set_keymap("n", "s", "<cmd>lua require('substitute').operator()<cr>", { noremap = true })
      vim.api.nvim_set_keymap("n", "ss", "<cmd>lua require('substitute').line()<cr>", { noremap = true })
      vim.api.nvim_set_keymap("n", "S", "<cmd>lua require('substitute').eol()<cr>", { noremap = true })
      vim.api.nvim_set_keymap("x", "s", "<cmd>lua require('substitute').visual()<cr>", { noremap = true })
    end,
    event = 'CursorMoved',
  },

  -- seeks ahead for textobjects. a=argument, l=previous, n=next (eg, ina=in next argument) and adds some useful ones.
  'wellle/targets.vim',
    -- }}},

    -- movements {{{

    {
      'numToStr/Comment.nvim', -- supports block commenting, although still not great support
      event = 'BufRead',
      config = function() require('plugin-configs.comment') end
    },

  {
    'szw/vim-maximizer',
    cmd = 'MaximizerToggle',
  },

  'pseewald/vim-anyfold', -- easy folding can just za at start of function and it'll fold the entire thing
    -- }}},

    -- completion {{{

    {
      'neovim/nvim-lspconfig',
      config = function() require'plugin-configs.lsp' end,
    },

    {
      'williamboman/mason-lspconfig.nvim',
    },

    {
      'akinsho/flutter-tools.nvim',
      dependencies = {
        'nvim-lua/plenary.nvim',
      },
    },

    -- commenting out to due slowness
    -- {
    --     'sigmasd/deno-nvim',
    --     config = function() require'deno-nvim'.setup() end,
    -- },

    {
      "williamboman/mason.nvim",
      config = function() require'plugin-configs.mason' end,
      after = 'mason-lspconfig.nvim'
    },

    {
      'j-hui/fidget.nvim',
      after = 'nvim-lspconfig',
      config = function() require"fidget".setup{} end
    },

    -- {
    --     'ojroques/nvim-lspfuzzy',
    --     dependencies = {
    --         {'junegunn/fzf'},
    --         {'junegunn/fzf.vim'},  -- to enable preview (optional)
    --     },
    --     config = function() require('lspfuzzy').setup {} end
    -- },

    {
      'ray-x/lsp_signature.nvim',
      config = function() require('plugin-configs.lsp_signature') end,
    },

    {
      'folke/lua-dev.nvim',
    },

    -- wait for more mature
    -- {
    --     'sidebar-nvim/sidebar.nvim',
    --     config = function()
    --         local sidebar = require("sidebar-nvim")
    --         local opts = {
    --             open = true,
    --             initial_width = 20,
    --             hide_statusline = true,
    --             sections = { "git", "diagnostics" },
    --         },
    --         sidebar.setup(opts)
    --     end
    -- },

    -- {
    --     'ms-jpq/coq_nvim',
    --     branch = 'coq',
    --     config = function() require'plugin-configs.coq' end,
    -- },

    {
      'danymat/neogen',
      config = function()
        require('neogen').setup{}
      end,
      after = 'nvim-treesitter'
    },

    {
      'hrsh7th/nvim-cmp',
      event = { 'InsertEnter', 'CmdlineEnter' },
      dependencies = {
        {
          'kdheepak/cmp-latex-symbols',
          after = 'nvim-cmp'
        },
        {
          'hrsh7th/cmp-nvim-lsp',
          after = 'nvim-cmp'
        },
        {
          'hrsh7th/cmp-buffer',
          after = 'nvim-cmp'
        },
        {
          'hrsh7th/cmp-path',
          after = 'nvim-cmp'
        },
        {
          'hrsh7th/cmp-cmdline',
          after = 'nvim-cmp'
        },
        {
          'hrsh7th/cmp-nvim-lua',
          after = 'nvim-cmp'
        },
        {
          'f3fora/cmp-spell',
          after = 'nvim-cmp'
        },
        {
          'saadparwaiz1/cmp_luasnip',
          after = 'nvim-cmp'
        },
        {
          'tzachar/cmp-tabnine',
          run='./install.sh',
          config = function() require'plugin-configs.compe-tabnine' end,
          after = 'nvim-cmp'
        },
      },
      config = function() require('plugin-configs.compe') end,
    },

    {
      'L3MON4D3/LuaSnip',
    },

    -- }}},

    -- debugging {{{

    {
      "folke/trouble.nvim",
      dependencies = "kyazdani42/nvim-web-devicons",
      config = function() require('plugin-configs.trouble') end,
      after = 'nvim-lspconfig'
    },

    {
      'MTDL9/vim-log-highlighting'
    },
  -- }}},

  -- {
  --     '~/Documents/projects/normandhy.nvim',
  --     config = function() require'normandhy'.setup() end
  -- },
})

require('plugin-configs.fasd')


-- local nnoremap = require('utils').nnoremap
-- nnoremap('<leader>rt', ":PlenaryBustedDirectory test/plenary/ {minimal_init = 'test/minimal_init.vim'}<CR>")
