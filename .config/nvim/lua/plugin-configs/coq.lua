local coq_settings = {
  auto_start = true,
  clients = {
	tabnine = {
	  enabled=true
	},
  },
}

vim.g.coq_settings = coq_settings
