local nnoremap = require('utils').nnoremap

require('trouble').setup{}

nnoremap('<leader>td', ':TroubleToggle workspace_diagnostics<CR>')
nnoremap('<leader>tl', ':TroubleToggle lsp_references<CR>')
