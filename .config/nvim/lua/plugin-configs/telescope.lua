
local nnoremap = require('utils').nnoremap
local actions = require('telescope.actions')
local actions_set = require('telescope.actions.set')
local sorters = require('telescope.sorters')
local themes = require('telescope.themes')

nnoremap('<Leader>b', ':lua require("telescope.builtin").find_files({ follow = true })<CR>')
nnoremap('<localleader>b', ':Telescope buffers<CR>')
nnoremap('<localleader>f', ':Telescope current_buffer_fuzzy_find<CR>')

-- note: currently no option to follow symlinks
nnoremap('<Leader>f', ':Telescope live_grep<CR>')
nnoremap('<Leader>m', ':Telescope marks<CR>')
nnoremap('<Leader>p', ':Telescope oldfiles<CR>') -- p=previous

require('telescope').setup{
	defaults = {
	    path_display = {'smart'},
		theme = "get_ivy",
		-- previewer = true,
		-- use_less = false, -- these two settings don't seem to do anything
		-- set_env = { ['COLORTERM'] = 'truecolor' },
		sort_lastused = true,
		layout_config = {
			prompt_position = 'bottom', -- broken for ivy
		},
		file_sorter = sorters.get_fzy_sorter,
		mappings = {
			i = {
				['<C-g>'] = actions.close,
				['<C-b>'] = actions.select_default,
				-- ['<C-b>'] = actions.move_selection_previous,
				['<C-u>'] = function() vim.cmd('normal! dd') end,
				['<C-j>'] = actions.move_selection_next,
				['<C-k>'] = actions.move_selection_previous,
			},
			n = {
				['<C-g>'] = actions.close,
			},
		},
		file_ignore_patterns = {
		    '^vendor',
		    '^node_modules',
		    -- flutter
		    'macos',
		    'windows',
		    'ios',
		    'linux',
		    'android',
		}
	}
}
