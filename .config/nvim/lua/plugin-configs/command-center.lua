
require("telescope").load_extension('command_center')

local command_center = require("command_center")

local noremap = {noremap = true}
command_center.add({
  {
    description = "Open command_center",
    command = "Telescope command_center",
    keybindings = {
      {"n", "<Leader>fc", noremap},
      {"v", "<Leader>fc", noremap},

      -- If ever hesitate when using telescope start with <leader>f,
      -- also open command center
      {"n", "<Leader>f", noremap},
      {"v", "<Leader>f", noremap},
    },
  }
}, command_center.mode.REGISTER_ONLY)

local silent_noremap = {noremap = true, silent = true}

local mappings = vim.api.nvim_get_keymap('n')
vim.tbl_map(
    function(elt)
        print(elt.desc)
        command_center.add({
            description = elt.desc,
            keybindings = elt.rhs
        })
    end,
    mappings
)

-- command_center.add({
--     {
--         description = "Search inside current buffer",
--         command = "Telescope current_buffer_fuzzy_find",
--         keybindings = { "n", "<leader>fl", noremap },
--     },  {
--         -- If no descirption is specified, command is used to replace descirption by default
--         -- You can change this behavior in settigns
--         command = "Telescope find_files",
--         keybindings = { "n", "<leader>ff", noremap },
--     }, {
--         -- If no keybindings specified, no keybindings will be displayed or registered
--         description = "Find hidden files",
--         command = "Telescope find_files hidden=true",
--     }, {
--         -- You can specify multiple keybindings for the same command ...
--         description = "Show document symbols",
--         command = "Telescope lsp_document_symbols",
--         keybindings = {
--             {"n", "<leader>ss", noremap},
--             {"n", "<leader>ssd", noremap},
--         },
--     }, {
--         -- ... and for different modes
--         description = "Show function signaure (hover)",
--         command = "lua vim.lsp.buf.hover()",
--         keybindings = {
--             {"n", "K", silent_noremap },
--             {"i", "<C-k>", silent_noremap },
--         }
--     }, {
--         -- If no command is specified, then this entry is ignored
--         description = "lsp run linter",
--         keybindings = "<leader>sf"
--     }
-- })
