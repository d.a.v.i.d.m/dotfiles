local nnoremap = require('utils').nnoremap
local add_prefix = require('utils').add_prefix
local remove_prefix = require('utils').remove_prefix

local saga = require("lspsaga")

saga.setup({
    code_action_lightbulb = {
        enable = false,
    }
})

-- NOTE: these overwrite lsp.lua's bindings
add_prefix('<lspleader>', 'lspsaga');
    nnoremap('h', ':Lspsaga hover_doc<CR>')
    nnoremap('ca', '<cmd>:Lspsaga code_action<CR>')
    nnoremap('rn', '<cmd>Lspsaga rename<CR>')
    nnoremap('lr', '<cmd>Lspsaga lsp_finder<CR>')
remove_prefix();


nnoremap('<C-n>', '<cmd>lua require("lspsaga.action").smart_scroll_with_saga(1)<CR>')
nnoremap('<C-p>', '<cmd>lua require("lspsaga.action").smart_scroll_with_saga(-1)<CR>')

nnoremap('<localleader>s', ':LSoutlineToggl<CR>')

