local nnoremap = require('utils').nnoremap

vim.api.nvim_set_var('better_whitespace_enabled',  0)

nnoremap('<f1>', ':StripWhitespace<CR>')

