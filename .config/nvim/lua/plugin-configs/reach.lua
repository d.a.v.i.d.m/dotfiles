local nnoremap = require('utils').nnoremap
local opts = { silent=true }

require('reach').setup({
    notifications = true
})

nnoremap('<leader>r', '', {
    callback = function() require('reach').buffers({handle = 'dynamic'}) end,
    desc = 'Reach buffer'
}, opts)
