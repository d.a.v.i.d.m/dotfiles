require('mini.base16').mini_palette('#112641', '#e2e98f', 75)

-- require('mini.comment')

require('mini.pairs').setup({})

require('mini.trailspace').setup({})

-- require('mini.tabline').setup({})

-- require('mini.starter').setup({})

-- require('mini.trailspace').setup({})

require('mini.surround').setup({}) -- find: sf/F, interactive: s<op>i, highlight: sh

require('mini.splitjoin').setup()

vim.cmd [[
	colorscheme minischeme
]]
