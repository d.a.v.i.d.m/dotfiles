vim.api.nvim_set_var('indentLine_enabled ',  1)
vim.api.nvim_set_var('indentLine_color_term', 239)
vim.api.nvim_set_var('indentLine_color_gui ',  '#4e4e4e')
vim.api.nvim_set_var('indentLine_char', '¦')

