local nnoremap = require('utils').nnoremap

require('aerial').setup()

nnoremap('<localleader>s', ':AerialToggle<CR>')

