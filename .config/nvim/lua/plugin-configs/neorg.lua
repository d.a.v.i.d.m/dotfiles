-- treesitter support. must run before neorg setup
-- local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

-- parser_configs = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg",
--         files = { "src/parser.c", "src/scanner.cc" },
--         branch = "main"
--     },
-- }

-- parser_configs_meta = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg-meta",
--         files = { "src/parser.c" },
--         branch = "main"
--     },
-- }

-- parser_configs_table = {
--     install_info = {
--         url = "https://github.com/nvim-neorg/tree-sitter-norg-table",
--         files = { "src/parser.c" },
--         branch = "main"
--     },
-- }


require('neorg').setup {
    -- Tell Neorg what modules to load
    load = {
        ["core.defaults"] = {}, -- Load all the default modules
        ["core.concealer"] = {
        }, -- Allows for use of icons
        ["core.dirman"] = { -- Manage your directories with Neorg
            config = {
                workspaces = {
                    nextcloud = "~/Nextcloud/"
                }
            }
        },
        ["core.completion"] = {
            config = {
                engine = "nvim-cmp"
            }
        },
        ["core.qol.toc"] = {},
        ["core.presenter"] = {
            config = {
                zen_mode = "zen-mode"
            }
        }
    },
}
