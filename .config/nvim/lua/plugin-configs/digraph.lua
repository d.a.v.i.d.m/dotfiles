-- Map double Ctrl-K in insert mode to search digraph names
map('i', '<C-K><C-K>', '<Plug>(DigraphSearch)', { noremap = false })

