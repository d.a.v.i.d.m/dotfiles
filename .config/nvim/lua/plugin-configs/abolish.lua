function createAbolishAbbreviations()

    vim.cmd[[
    Abolish -buffer abt about
    Abolish -buffer acc{,y} according{,ly}
    Abolish -buffer amt{,s,d} amount{,s,ed}
    Abolish -buffer appr{,s,g} approach{,es,ing}
    Abolish -buffer assoc{,d,n,ns,g} associat{e,ed,ion,ions,ing}
    Abolish -buffer avg average
    Abolish -buffer bc because
    Abolish -buffer betw between
    Abolish -buffer cont{,y} continuous{,ly}
    Abolish -buffer def{,n,d} defin{e,ition,ed}
    " Abolish -buffer det{,s} detail{,s}
    Abolish -buffer dev{,s,d,g,t,p} develop{er,ers,ed,ing,ment,}
    Abolish -buffer dict{,s} dictionar{y,ies}
    Abolish -buffer diff{,y,s,t,ce} differen{t,tly,ces,t,ce}
    Abolish -buffer elt{,s} element{,s}
    Abolish -buffer exp experience
    Abolish -buffer fn{,s,y} function{,s,ally}
    Abolish -buffer govt{,s} government{,s}
    " Abolish -buffer id{,s} identit{y,ies}
    Abolish -buffer imp important
    Abolish -buffer info information
    Abolish -buffer lang{,s} language{,s}
    Abolish -buffer lvl{,s} level{,s}
    Abolish -buffer mech mechanical
    Abolish -buffer mem{,s} memor{y,ies}
    Abolish -buffer col column
    Abolish -buffer nat{,y,e,l} nat{ure,urally,ure,ural}
    Abolish -buffer nec{,y} necessar{y,ily}
    Abolish -buffer poss{,y,ty} possib{le,ly,ility}
    Abolish -buffer ppl{,s} people{,'s}
    Abolish -buffer probm{,s} problem{,s}
    Abolish -buffer cmd{,s,d} command{,s,ed}
    Abolish -buffer proby probably
    Abolish -buffer proc{,s} process{,es}
    Abolish -buffer prod{,s,n} produc{e,es,tion}
    Abolish -buffer recog{,l,s,y,d} recogniz{,able,es,ably,ed}
    Abolish -buffer repc{,d,s,n} reproduc{e,ed,es,tion}
    Abolish -buffer rep{,d,s,n,ns,nt,t} represent{ative,ed,s,ation,ations,,}
    Abolish -buffer repl{,d,s,g} replac{e,ed,es,ing}
    Abolish -buffer sep{,n,d,y} separat{e,ion,ed,ely}
    Abolish -buffer intro{d,n,s} intro{duced,duction,duces}
    Abolish -buffer soc{,y} social{,ly}
    iabbrev A: _A_:
    iabbrev pov point of view
    iabbrev povs points of view
    iabbrev Q: _Q_:
    Abolish -buffer varn{,s} variation{,s}
    " Abolish -buffer pop popular
    Abolish -buffer popn{,s} population{,s}
    Abolish -buffer prev previous
    Abolish -buffer civ{,s} civilization{,s}
    Abolish -buffer evol{,s} evolution{,s}
    Abolish -buffer desc{,s} decision{,s}
    Abolish -buffer pwr{,s,d} power{,s,ed}
    Abolish -buffer bef before
    Abolish -buffer relg{n,ns,s} religio{n,ns,us}
    Abolish -buffer ritl{,s} ritual{,s}
    Abolish -buffer ind{,s} individual{,s}
    Abolish -buffer virt{,y} virtual{,ly}
    Abolish -buffer genr{,s,d,g} generat{e,es,ed,ing}
    " Abolish -buffer pt{,s,d,r} point{,s,ed,er}
    Abolish -buffer cond{,s,d,g} condition{,s,ed,ing}
    Abolish -buffer req{,s,d,t,ts,g} requir{e,es,ed,ement,ements,ing}
    " iabbrev abbrs abbreviations
    Abolish -buffer mins minutes
    Abolish -buffer loc{n,ns} location{,s}
    Abolish -buffer addr{,s,d} address{,es,ed}
    Abolish -buffer bio{,cl,st,sts} biolog{y,ical,ist,ists}
    Abolish -buffer org{,s,d} organiz{ation,ations,ed}
    Abolish -buffer expr{,s} expression{,s}
    Abolish -buffer freq{,s,y} frequen{t,cies,tly}
    Abolish -buffer inst{,s} institution{,s}
    Abolish -buffer circ{,s,y} circuit{,s,ry}
    Abolish -buffer alg{,s,y} algorithm{,s,ically}
    Abolish -buffer anth{,s,l,st,cl} anthropolog{y,ists,ical,ist,ical}
    Abolish -buffer rel{,d,v,p,ps,g} relat{ion,ed,ive,ionship,ionships,ing}
    Abolish -buffer ling{,s,c,cs} linguist{,s,ic,ics}
    Abolish -buffer archae{,t,ts,l} archaeolog{y,ist,ists,ical}
    Abolish -buffer distro{,s} distribution{,s}
    Abolish -buffer op{,s} operation{,s}
    Abolish -buffer instr{,s} instruction{,s}
    Abolish -buffer dep{,s,cy,cys} depend{,s,ency,encies}
    " Abolish -buffer dist{,s,d} distance{,s,d}
    Abolish -buffer fmt{,s,d} format{,s,ted}
    Abolish -buffer thm{,s} theorem{,s}
    Abolish -buffer detr{,s,d} determine{,s,d}
    Abolish -buffer dept{,s} department{,s}

    iabbrev thet θ
    iabbrev om Ω
    ]]
end

function createSpecializedAbolishAbbrevations()
    -- can appear in code, reminder to never enable when programming
    vim.cmd[[
    Abolish -buffer apt{,s} apartment{,s}
    Abolish -buffer num{,s} number{,s}
    Abolish -buffer sys{,s} system{,s}
    Abolish -buffer std{,s,z,zd,n} standard{,s,ize,ized,ization}
    Abolish -buffer val{,s} value{,s}
    Abolish -buffer attr{,s,d} attribute{,s,d}
    Abolish -buffer img{,s} image{,s}
    Abolish -buffer arg{,s} argument{,s}
    Abolish -buffer div{,n,d} divi{de,sion,ded}
    Abolish -buffer obj{,s,y} object{,s,ify}
    Abolish -buffer env{,s} environment{,s}
    Abolish -buffer var{,s} variable{,s}
    Abolish -buffer doc{,s,n} document{,s,ation}
    Abolish -buffer calc{,s,l,ls,g} calculat{e,es,or,ors,ing}
    Abolish -buffer strat{,s} strateg{y,ies}
    Abolish -buffer wic{,s,t} witch{,es,craft}
    Abolish -buffer incl{,s,d,g} includ{e,es,ed,ing}

    Abolish -buffer eps έ
    Abolish -buffer delt δ
    Abolish -buffer un ∪
    Abolish -buffer alph α
    Abolish -buffer sig Σ
    Abolish -buffer gam Γ
    ]]
end

vim.cmd[[
augroup CreateAbolish
autocmd!
autocmd Filetype markdown,text,latex,norg lua require('plugin-configs.abolish').createAbolishAbbreviations()
autocmd Filetype markdown,text,latex,norg lua require('plugin-configs.abolish').createSpecializedAbolishAbbrevations()
augroup END
]]

return {
    createAbolishAbbreviations = createAbolishAbbreviations,
    createSpecializedAbolishAbbrevations = createSpecializedAbolishAbbrevations,
}


