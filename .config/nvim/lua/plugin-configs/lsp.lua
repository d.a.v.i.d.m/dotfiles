local add_prefix = require('utils').add_prefix
local remove_prefix = require('utils').remove_prefix
local nnoremap = require('utils').nnoremap

local function renameFileWithPrompt()

    vim.ui.input(
        {
            prompt = 'Move '..vim.fn.expand('%:t')..' to: ',
            completion = 'dir'
        },
        function(input)

            if input == nil then
                vim.notify('Aborted rename', vim.log.levels.INFO);
                return;
            end

            local currentDir = vim.fn.expand('%:h')
            local newLocation = currentDir..'/'..input;
            vim.notify('\n'..newLocation, vim.log.levels.INFO)

            local currentFile = vim.api.nvim_buf_get_name(0);
            vim.lsp.util.rename(currentFile, newLocation)
        end
    )
end



-- NOTE: these mappings are overwritten by lspsaga.lua's bindings. keeping here as fallback

add_prefix('<lspleader>', 'lsp')
    -- goes to local definition (eg, parameter)
    nnoremap('gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
    -- goes to overarching type definition (eg, class definition)
    nnoremap('gt', '<cmd>lua vim.lsp.buf.type_definition()<CR>')
    nnoremap('gD', '<cmd>lua vim.lsp.buf.declaration()<CR>')

    nnoremap('rn', '<cmd>lua vim.lsp.buf.rename()<CR>')
    nnoremap('ca', '<cmd>lua vim.lsp.buf.code_action()<CR>')
    -- nnoremap('<lspleader>ca', '<cmd>CodeActionMenu<CR>')
    nnoremap('lr', '<cmd>lua vim.lsp.buf.references()<CR>')
    nnoremap('frn', '', {
        callback = renameFileWithPrompt,
        desc = 'Rename file'
    })

    nnoremap('h', '<cmd>lua vim.lsp.buf.hover()<CR>')
    nnoremap('e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>')
    nnoremap('d', '<cmd>TroubleToggle<CR>')

    nnoremap('=', '<cmd>lua vim.lsp.buf.formatting()<CR>')
remove_prefix()

-- require'lspconfig'.dartls.setup{}

-- usecase is more quickly cycling through, so don't want to
-- bog down by adding <lspleader>
-- nnoremap('<C-k>', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
-- nnoremap('<C-j>', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>')



-- open the target of a textDocument/definition request in a floating window
--[[
local function preview_location_callback(_, _, result)
  if result == nil or vim.tbl_isempty(result) then
    return nil
  end
  vim.lsp.util.preview_location(result[1])
end

function PeekDefinition()
  local params = vim.lsp.util.make_position_params()
  return vim.lsp.buf_request(0, 'textDocument/definition', params, preview_location_callback)
end
--]]

-- potentially add https://github.com/neovim/nvim-lspconfig/wiki/Snippets
