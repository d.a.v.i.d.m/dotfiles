local nnoremap = require('utils').nnoremap
local add_prefix = require('utils').add_prefix
local remove_prefix = require('utils').remove_prefix

add_prefix('<leader>m', 'harpoon')

nnoremap('a', function()
    require('harpoon.mark').add_file()
end)

nnoremap('l', function()
    require('harpoon.ui').toggle_quick_menu()
end)

nnoremap('n', function()
    require('harpoon.ui').nav_next()
end)

nnoremap('p', function()
    require('harpoon.ui').nav_prev()
end)

remove_prefix()
