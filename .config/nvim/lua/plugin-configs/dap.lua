local nnoremap = require('utils').nnoremap
local dap = require('dap')

nnoremap("<F4>", ":lua require('dapui').toggle()<CR>")
nnoremap("<F5>", ":lua require('dap').toggle_breakpoint()<CR>")
nnoremap("<F9>", ":lua require('dap').continue()<CR>")

nnoremap("<F1>", ":lua require('dap').step_over()<CR>")
nnoremap("<F2>", ":lua require('dap').step_into()<CR>")
nnoremap("<F3>", ":lua require('dap').step_out()<CR>")

nnoremap("<Leader>dsc", ":lua require('dap').continue()<CR>")
nnoremap("<Leader>dsv", ":lua require('dap').step_over()<CR>")
nnoremap("<Leader>dsi", ":lua require('dap').step_into()<CR>")
nnoremap("<Leader>dso", ":lua require('dap').step_out()<CR>")

nnoremap("<Leader>dhh", ":lua require('dap.ui.variables').hover()<CR>")
nnoremap("<Leader>dhv", ":lua require('dap.ui.variables').visual_hover()<CR>")

nnoremap("<Leader>duh", ":lua require('dap.ui.widgets').hover()<CR>")
nnoremap("<Leader>duf", ":lua local widgets=require('dap.ui.widgets');widgets.centered_float(widgets.scopes)<CR>")

nnoremap("<Leader>dro", ":lua require('dap').repl.open()<CR>")
nnoremap("<Leader>drl", ":lua require('dap').repl.run_last()<CR>")

nnoremap("<Leader>dbc", ":lua require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>")
nnoremap("<Leader>dbm", ":lua require('dap').set_breakpoint({ nil, nil, vim.fn.input('Log point message: '))<CR>")
nnoremap("<Leader>dbt", ":lua require('dap').toggle_breakpoint()<CR>")

nnoremap("<Leader>dc", ":lua require('dap.ui.variables').scopes()<CR>")
nnoremap("<Leader>di", ":lua require('dapui').toggle()<CR>")

