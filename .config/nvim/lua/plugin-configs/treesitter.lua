-- https://github.com/vhyrro/neorg
local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

parser_configs.norg = {
    install_info = {
        url = "https://github.com/vhyrro/tree-sitter-norg",
        files = { "src/parser.c" },
        branch = "main"
    },
}


require'nvim-treesitter.configs'.setup {
    ignore_install = { "dartls" },
    highlight = {
        enable = true,
    },
    indent = {
        enable = true,
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "gnn",
            node_incremental = "grn",
            scope_incremental = "grc",
            node_decremental = "grm",
        }
    },
    textobjects = {
        select = {
            enable = true,
            lookahead = true,
            keymaps = {
                ["af"] = "@function.outer",
                ["if"] = "@function.inner",
                -- ["ac"] = "@class.outer",
                -- ["ic"] = "@class.inner",
                ["ip"] = "@parameter.inner",
                ["ap"] = "@parameter.outer",
                ["ic"] = "@scopename.inner",
                ["is"] = "@statement.outer",
            },
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                ["<localleader>jfs"] = "@function.outer",
                ["<localleader>jcs"] = "@call.inner",
                ["<localleader>jps"] = "@parameter.inner",
                ["<localleader>jbs"] = "@block.outer",
            },
            goto_next_end = {
                ["<localleader>jfe"] = "@function.outer",
                ["<localleader>jce"] = "@call.inner",
                ["<localleader>jpe"] = "@parameter.inner",
                ["<localleader>jbe"] = "@block.outer",
            },
            goto_previous_start = {
                ["<localleader>kfs"] = "@function.outer",
                ["<localleader>kcs"] = "@call.inner",
                ["<localleader>kps"] = "@parameter.inner",
                ["<localleader>kbs"] = "@block.outer",
            },
            goto_previous_end = {
                ["<localleader>kfe"] = "@function.outer",
                ["<localleader>kce"] = "@call.inner",
                ["<localleader>kpe"] = "@parameter.inner",
                ["<localleader>kbe"] = "@block.outer",
            },
        },

    }
}


vim.cmd[[
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
]]


local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.rexc = {
    install_info = {
        url = "~/Documents/projects/tree-sitter-rexc", -- local path or git repo
        files = {"src/parser.c"},
    },
    filetype = "rx", -- if filetype does not match the parser name
}
vim.opt.runtimepath:append(',~/Desktop/delete')
