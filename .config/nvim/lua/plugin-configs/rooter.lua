-- :RooterToggle
vim.api.nvim_set_var('rooter_manual_only ',  1)
vim.api.nvim_set_var('rooter_patterns ',  {'.git', 'Makefile', 'README.md'})

