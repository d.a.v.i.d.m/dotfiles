local nnoremap = require('utils').nnoremap

vim.api.nvim_set_var('undotree_WindowLayout', 2)
vim.api.nvim_set_var('undotree_SetFocusWhenToggle', 1)

nnoremap('<localleader>u', ':UndotreeToggle<CR>')

