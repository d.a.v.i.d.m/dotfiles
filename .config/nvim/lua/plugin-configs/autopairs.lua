-- note: for some reason autopairs seems to still work, but if not, maybe use https://github.com/windwp/nvim-autopairs
local inoremap = require('utils').inoremap
local npairs = require('nvim-autopairs')

npairs.setup()

--
-- nvim-compe integration
--

-- skip it, if you use another global object
_G.MUtils= {}

vim.g.completion_confirm_key = ""
MUtils.completion_confirm=function()
  if vim.fn.pumvisible() ~= 0  then
    if vim.fn.complete_info()["selected"] ~= -1 then
      return vim.fn["compe#confirm"](npairs.esc("<cr>"))
    else
      return npairs.esc("<cr>")
    end
  else
    return npairs.autopairs_cr()
  end
end


inoremap('<CR>','v:lua.MUtils.completion_confirm()', { expr = true })

