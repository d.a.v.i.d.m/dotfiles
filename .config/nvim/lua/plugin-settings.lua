--"" Plugin settings """

-- NOTE: abbrs and vim-abolish are in after/plugin/abolish.vim

vim.cmd [[
	filetype plugin indent on
	syntax enable
]]

