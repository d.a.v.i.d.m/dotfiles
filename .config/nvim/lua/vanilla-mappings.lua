-- couldn't figure out a way to unpack better
local inoremap = require('utils').inoremap
local nnoremap = require('utils').nnoremap
local cnoremap = require('utils').cnoremap
local vnoremap = require('utils').vnoremap
local tnoremap = require('utils').tnoremap
local add_custom_leader = require('utils').add_new_custom_leader

--""""""""""""""""""""""""""
--   Key Mappings          "
--""""""""""""""""""""""""""

-- leader is for general use, localleader is for things which deal with buffers
-- and their contents in a more abstract/vim-like manner, per buffer
vim.g.mapleader = ","
vim.g.maplocalleader = " "

add_custom_leader('lspleader', '<localleader><localleader>')

nnoremap('h', ':')
cnoremap('hh', '<CR>')

nnoremap('<localleader>jd', '', {
    callback = function() vim.diagnostic.goto_next() end,
    desc = 'goto next diagnostic'
})

nnoremap('<localleader>kd', '', {
    callback = function() vim.diagnostic.goto_prev() end,
    desc = 'goto prev diagnostic'
})
-- nnoremap('jd', )
-- stretch my pinky less
nnoremap('<C-e>', '<Enter>', { noremap = false });
--inoremap('<C-e>', '<Enter>', { noremap = false });
vnoremap('<C-e>', '<Enter>', { noremap = false });
cnoremap('<C-e>', '<Enter>', { noremap = false });

-- nnoremap('<Leader>rs', ':w | source % | PackerSync <CR>')

-- make j and k go by screen lines not file lines
-- noremap <silent> <expr> k (v:count ? ':set scrolloff=999<CR> k :set scrolloff=0<CR>' : 'gk')

nnoremap('k', "(v:count ? 'k' : 'gk')", { expr = true })
nnoremap('j', "(v:count ? 'j' : 'gj')", { expr = true })

-- spellcheck, just auto sub in first suggestion
nnoremap('zz', '1z=e')

nnoremap('<C-s>', '[s')
nnoremap('<Space>z', 'mz<C-s>zz`z', { noremap = false })

inoremap('jk', '<Esc>l')
cnoremap('jk', '<Esc>l')

inoremap('eu', '<Esc>l')
cnoremap('eu', '<Esc>l')

nnoremap('Y', 'y$')

-- note: this section overwrites registers/markes -----
--
nnoremap('yy', 'myyy')

---------------

nnoremap(';', ':')
vnoremap(';', ':')

nnoremap(':', ';')
vnoremap(':', ';')

nnoremap("'", "`")
nnoremap("`", "'")

nnoremap("<localleader>t", ":b#<CR>", { silent = true })
nnoremap("<localleader>o", ":only<CR>", { silent = true })

-- nnoremap("e", "%")
-- vnoremap("e", "%")

-- Q for formatting, don't use ex mode. honestly, I never use gq either lol
nnoremap('Q', 'gq')

-- buffer jumping
nnoremap('<localleader>j', ':bprev<CR>', { silent = true })
nnoremap('<localleader>k', ':bnext<CR>', { silent = true })
nnoremap('<localleader><localleader>', ':b#<CR>', { silent = true })
nnoremap('<C-n>', ':b#<CR>', { silent = true })
nnoremap('<localleader>g', ':set autochdir<CR>')

nnoremap('<localleader>w', '<C-w>', { silent = true })

-- tnoremap('jk', '<C-\><C-n>')
tnoremap('eu', "<C-\\><C-n>")

-- tnoremap('nvim', '<C-\><C-n>:edit')
-- tnoremap('vim', '<C-\><C-n>:edit')

-- provides continuity when mixed with `set scrolloff=999`
-- nnoremap('<C-d>', '<C-d>k')
-- nnoremap('<C-u>', '<C-u>j')

-- very magic regex, more normal matching
--map('n', '/', '/\v', { noremap = false });
--map('n', '?', '?\v', { noremap = false });
--cnoremap('s/', 's/\v');

--"" readline/emacs-like. partly taken from vim-rsi
inoremap('<C-a>', '<C-o>^')
cnoremap('<C-a>', '<Home>')

inoremap('<C-e>', '<End>')

inoremap('<M-k>', '<C-o>d)')
inoremap('<M-d>', '<C-o>dW')

inoremap('<C-g>', '<Esc>')
cnoremap('<C-g>', '<Esc>')
vnoremap('<C-g>', '<Esc>')
nnoremap('<C-g>', ':q<CR>')


inoremap('<silent> <C-v> <Esc>:set paste<CR>pl:set', 'nopaste<CR>a')
cnoremap('<silent> <C-v> <Esc>:set paste<CR>pl:set', 'nopaste<CR>a')

-- now i don't have to use arrow keys for command history!!!! so happy i thought of this
-- also i just realized i _severely_ underuse <C-o>
-- note: two ups, bc usually i save (:w) and then want to run the previous
-- command on the text i just saved
inoremap('<C-u>', '<C-o>dd')


-- scrolls so next section is what's visible, preserving scrolloff
nnoremap('gs', ':set scrolloff=0<CR>}jztM:set scrolloff=999<CR>')

-- todo: put into autocmd for vim files
-- nnoremap('<C-s>', ':w<CR>:so %<CR>', { buffer: true })

nnoremap('<leader>tp', '<cmd>set filetype=rx<CR><cmd>TSPlayground<CR>')

nnoremap('<localleader>u', '<Esc>Go<Esc>:MoveMentionToBottom ')

-- https://vi.stackexchange.com/questions/7368/how-can-i-type-the-alphabetic-subscripts
-- use as <C-k>ns and <C-k>nS for ₙ and ⁿ respectively
--alphsubs ---------------------- {{{
vim.cmd [[
	execute "digraphs ks " . 0x2096
	execute "digraphs as " . 0x2090
	execute "digraphs es " . 0x2091
	execute "digraphs hs " . 0x2095
	execute "digraphs is " . 0x1D62
	execute "digraphs ks " . 0x2096
	execute "digraphs ls " . 0x2097
	execute "digraphs ms " . 0x2098
	execute "digraphs ns " . 0x2099
	execute "digraphs os " . 0x2092
	execute "digraphs ps " . 0x209A
	execute "digraphs rs " . 0x1D63
	execute "digraphs ss " . 0x209B
	execute "digraphs ts " . 0x209C
	execute "digraphs us " . 0x1D64
	execute "digraphs vs " . 0x1D65
	execute "digraphs xs " . 0x2093
]]
--}}}

-- rename terminal buffer {{{
vim.cmd [[
  function! RenameTerminalBufferToCurrentCommand()

    " unable to access $HISTFILE from vim, so change this variable to your history file
    let l:historyFile = "~/.zsh_history"
    let l:mostRecentCommand = system("tail -1 " . l:historyFile . " | cut -f2- -d\\;")

    let l:newFileName = "term " . fnameescape(trim(l:mostRecentCommand))

    " the keepalt stops :file from creating an alternative file (alt files are
    " annoying when buffer switching)
    " :file renames the buffer
    silent! execute "keepalt file " . l:newFileName

  endfunction

  tnoremap <silent> <Enter> <Enter><C-\><C-n>:call RenameTerminalBufferToCurrentCommand()<Enter>a
]]
-- }}}

