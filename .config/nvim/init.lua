
-- modified from https://github.com/vhyrro/neovhy/blob/main/init.lua for performance
vim.cmd [[
	syntax off
	filetype off
	filetype plugin indent off
]]

local rtp = vim.opt.runtimepath:get()
vim.opt.runtimepath = ''
vim.opt.shadafile = 'NONE'

-- disable builtin vim plugins
vim.g.loaded_gzip = false
vim.g.loaded_tar = false
vim.g.loaded_tarPlugin = false
vim.g.loaded_zipPlugin = false
vim.g.loaded_2html_plugin = false
vim.g.loaded_netrw = false
vim.g.loaded_netrwPlugin = false
vim.g.loaded_matchit = false
vim.g.loaded_matchparen = false
vim.g.loaded_spec = false
vim.g.loaded_remote_plugins = false

local function bootstrap_package_manager()
  local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
  if not (vim.uv or vim.loop).fs_stat(lazypath) then
    vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "https://github.com/folke/lazy.nvim.git",
      "--branch=stable", -- latest stable release
      lazypath,
    })
  end
  vim.opt.rtp:prepend(lazypath)
end

vim.schedule(function()

  vim.opt.shadafile = ''
 	vim.opt.runtimepath = rtp

  bootstrap_package_manager()

  require('vanilla-mappings')

  require('misc')

  require('settings')

  require('plugins')
  require('plugin-mappings')

  vim.cmd [[
		rshada!
		doautocmd BufRead
		syntax on
		filetype on
		filetype plugin indent on
	]]
end)


