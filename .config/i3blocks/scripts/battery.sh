#!/bin/bash

is_charging() {

	discharging=$(upower -d | grep -E 'state:.*discharging')

	if [ "$discharging" == '' ]; then
		echo 1;
	else
		echo 0;
	fi
}

get_time_until_charged() {
	# upower has time to full for multiple batteries, unlike acpi

	# if multiple batteries are partly drained
	time_until_full=$(upower -d | sed '/BAT/,/Device/!d' | grep 'time to full' | grep -Eo '[0-9].*')

	if [ "$time_until_full" == null ]
	then
		return 0;
	fi

	seconds_until_full=0;

	# loop over each battery
	while read -r line; do

		if $( echo $line | grep -q 'minutes' )
		then
			minutes_until_full=$(echo $line | grep -Eo '[0-9.]+');
			seconds_until_full=$(bc <<< "$seconds_until_full + ($minutes_until_full * 60)")
		elif $( echo $line | grep -q 'hours' )
		then
			hours_until_full=$(echo $line | grep -Eo '[0-9.]+');
			seconds_until_full=$(bc <<< "$seconds_until_full + ($hours_until_full * 60 * 60)")
		fi
	done <<< $time_until_full;


	pretty_time=$(date -u -d @${seconds_until_full} +%T);

	if [ $(echo "$seconds_until_full < 1800" | bc) ] # 30 minutes
	then
		color="#00FF00"
	elif [ $(echo "$seconds_until_full < 5400" | bc) ] # 1.5 hours
	then
		color="#FFFF00"
	else
		# color="#FF0000"
		color="#FFFFFF"
	fi

	echo "<span foreground=\"$color\">$pretty_time</span> until full";
}

get_time_until_empty() {

	# note: I'm just taking the last line of upower, but
	# not sure abt that bc I have multiple batteries
	time_until_empty=$(upower -d | grep 'time to empty' | grep -Eo '[0-9].*' | tail -1)

	echo empty in $time_until_empty;
	# sum_remaining_charge=$(acpitool -B | grep -E 'Remaining capacity' | awk '{print $4}' | grep -Eo "[0-9]+" | paste -sd+ | bc);
	# present_rate=$(acpitool -B | grep -E 'Present rate' | awk '{print $4}' | grep -Eo "[0-9]+" | paste -sd+ | bc);

	# if [ $(echo "$present_rate < 1" | bc) -eq 1 ]
	# then
	# 	present_rate=1
	# fi

	# seconds=$(bc <<< "scale = 10; ($sum_remaining_charge / $present_rate) * 3600");

	# pretty_time=$(date -u -d @${seconds} +%T);

	# color="#00FF00"
	# if [ $(echo "$seconds < 2700" | bc) -eq 1 ] # 45 minutes
	# then
	# 	color="#FFAE00"
	# elif [ $(echo "$seconds < 1200" | bc) -eq 1 ] # 20 minutes
	# then
	# 	color="#FF0000"
	# fi

	# if [ $(echo "$seconds < 600" | bc) -eq 1 ] # 10 minutes
	# then
	# 	echo "<span foreground=\"red\">$pretty_time LOW TIME</span> remaining";
	# else
	# 	echo "<span foreground=\"$color\">$pretty_time</span> remaining";
	# fi

	# if [ $(echo "$seconds_until_full < 420" | bc) -eq 1 ] # 7 minutes. thus, 420
	# then
	# 	notify-send "Low Battery" "Low Battery Alert 2. Please plug in your device." -u critical
	# fi
	# return seconds;
}

get_time() {

	if [[ "$(is_charging)" == 1 ]]; then
		get_time_until_charged;
	else
		get_time_until_empty;
	fi
}

send_battery_notification() {

	percent=$(get_percent)

    lastNotification=`cat /tmp/custom-i3-bat.dat`

	if [ $(echo "$percent < 3" | bc) -eq 1 ] && [ "$lastNotification" != "3%" ]
	then
		notify-send "WARNING: 3% battery left" -u critical
        echo "3%" > /tmp/custom-i3-bat.dat
	elif [ $(echo "$percent < 5" | bc) -eq 1 ] && [ "$lastNotification" != "5%" ]
	then
		notify-send "WARNING: 5% battery left" -u critical
        echo "5%" > /tmp/custom-i3-bat.dat
	elif  [ $(echo "$percent < 10" | bc) -eq 1 ] && [ "$lastNotification" != "10%" ] 
	then
		notify-send "WARNING: 10% battery left ->$lastNotification" -t 7000
        echo "10%" > /tmp/custom-i3-bat.dat
    elif [ $(echo "$percent >= 10" | bc) -eq 1 ]
    then
        echo "" > /tmp/custom-i3-bat.dat
	fi

}

get_percent() {
	echo $(upower -d | grep 'percentage' | grep -Eo '[0-9.]+' | tail -1)
}

get_battery_combined_percent() {
	# acpitool has fractional percent, unlike upower

	# total_charge=$(acpitool -b | awk '{print $5}' | grep -Eo "[0-9.]+" | paste -sd+ | bc);
	# battery_number=$(acpitool -b | wc -l);
	# percent=$(bc <<< "scale = 2; $total_charge / $battery_number");

    icon=""
	percent=$(get_percent)

	if  [ $(echo "$percent < 75" | bc) -eq 1 ]
	then
		icon=""
	elif  [ $(echo "$percent < 50" | bc) -eq 1 ]
	then
		icon=""
	elif  [ $(echo "$percent < 25" | bc) -eq 1 ]
	then
		icon=""
	fi

	color="#00FF00"

	if  [ $(echo "$percent < 20" | bc) -eq 1 ]
	then
		color="#FF0000";
	elif  [ $(echo "$percent < 40" | bc) -eq 1 ]
	then
		color="#FFAE00";
	elif  [ $(echo "$percent < 60" | bc) -eq 1 ]
	then
		color="#FFF600";
	elif  [ $(echo "$percent < 85" | bc) -eq 1 ]
	then
		color="#A8FF00";
	fi

	if  [ $(echo "$percent < 10" | bc) -eq 1 ]
	then
		icon=""
		echo "<span foreground=\"red\">$icon $percent% LOW BATTERY</span>";

	else
		echo "<span foreground=\"$color\">$icon  $percent%</span>";
	fi

}



get_battery_charging_status() {

	present_rate=$(acpitool -B | grep -E 'Present rate' | awk '{print $4}' | grep -Eo "[0-9]+" | paste -sd+ | bc);

	if [ $present_rate -eq 0 ]
	then
		status="Full"
		status_color="#00FF00"

	elif [[ "$(is_charging)" == 0 ]]
	then
		status=""
		status_color="#CCCCCC"
	else # acpitool can give Unknown or Charging if charging, https://unix.stackexchange.com/questions/203741/lenovo-t440s-battery-status-unknown-but-charging
		status="⚡"
		status_color="#FFEA00"
	fi

	echo "<span foreground=\"$status_color\">$status</span>";
}

if [ "$(is_charging)" == 0 ]
then
  send_battery_notification
fi

echo "$(get_battery_charging_status) $(get_battery_combined_percent), $(get_time)";

