#!/bin/bash

if ifconfig -s | grep -q tun > /dev/null
then
	echo "<span foreground=\"#00FF00\">openVPN</span>";
elif sudo wg show | grep peer: > /dev/null
then
	echo "<span foreground=\"#00FF00\">wgVPN</span>";
else
	echo "<span foreground=\"red\">VPN</span>";
fi
