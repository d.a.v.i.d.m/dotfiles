#!/bin/bash

get_title_info() {
	echo $(playerctl metadata artist) - $(playerctl metadata title)
}

if [[ $(playerctl status) == "Playing" ]]; then

	echo  $(get_title_info)
else
	echo  $(get_title_info)
fi

