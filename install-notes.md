- Chromium flash: aur pepper-flash
- echo "HandleLidSwitch=suspend" >> /etc/systemd/logind.conf
- `qt5ct` and select kvantum-dark
- For PIA, see https://helpdesk.privateinternetaccess.com/hc/en-us/articles/219438247-Installing-OpenVPN-PIA-on-Linux
	- The tcp worked
	- Let regular user access it by doing
		- `sudo visudo`
		- `$USER ALL=(ALL:ALL) /usr/bin/openvpn`
-
	- sudo chown root:sys /sys/class/backlight/intel_backlight/brightness
	- sudo chmod g+w /sys/class/backlight/intel_backlight/brightness
	- sudo chown root:audio /usr/bin/alsamixer
- Typescript ctags: npm install --global git+https://github.com/Perlence/tstags.git
- npm i -g tldr
https://wiki.archlinux.org/index.php/GNOME/Keyring

- pip install --user httplib2
	- For calcurse caldav
	- Nvm. Can't handle multiple cardavs

- Create private-info
	- export ZIP=zip_code

- git clone https://github.com/tmux-plugins/tpm $XDG_CACHE_HOME/tmux/plugins/tpm
- press `o` in lynx, set to VI keybindings
- https://wiki.archlinux.org/index.php/Pi-hole#Pi-hole_Standalone
- https://wiki.archlinux.org/index.php/TLP#ThinkPads_only

- Since firefox profiles are random, can't auto symlink, so must do ~/.mozilla/firefox/your-profile/chrome/userChrome.css yourself 
	- example: `ln -s /home/user/dotfiles/chrome/userChrome.css /home/user/.mozilla/firefox/71wvllm2.dev-edition-default/chrome/userChrome.css`
	
	
xdg-mime default org.pwmt.zathura.desktop application/pdf


https://wiki.archlinux.org/title/Pacman#Enabling_parallel_downloads

abook
